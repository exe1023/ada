#include <stdio.h>
#include <string.h>

#define maxn 1001

int adjmatrix1[maxn][maxn];
int adjmatrix2[maxn][maxn];
int degree1[maxn];
int degree2[maxn];
int pi[maxn];

int T , n , m;

int inv1[maxn];
int inv2[maxn];

int discovered1[maxn];
int discovered2[maxn];

bool dfs(int now)
{
	discovered1[now] = 1;

	int discover_tmp1[maxn];
	int discover_tmp2[maxn];
	int pi_tmp[maxn];
	memcpy(discover_tmp1 , discovered1 , sizeof(int) * (n + 1) );
	memcpy(discover_tmp2 , discovered2 , sizeof(int) * (n + 1) );
	memcpy(pi_tmp , pi , sizeof(int) * (n + 1) );
	for(int i = 1 ; i <= n ; i++)
	{
		if((inv1[now] != inv2[i]) || (discovered2[i] != 0)) // find point
			continue;
		pi[now] = i;
		discovered2[i] = 1;

		for(int j = 1 ; j <= n ; j++ )// check isomorphism
		{
			if(adjmatrix1[now][j] >= 1 && pi[j] != 0)
			{
				if(adjmatrix2[i][pi[j]] != adjmatrix1[now][j])
				{
					pi[now] = 0;
					discovered2[i] = 0;
					break;
				}
			}
		}
		if(pi[now] == 0)
			continue;

		for(int j = 1 ; j <= n ; j++) // dfs
		{
			if(adjmatrix1[now][j] >= 1 && discovered1[j] == 0)
			{
				if(dfs(j) == false)
				{
					memcpy(discovered1 , discover_tmp1 , sizeof(int) * (n + 1) );
					memcpy(discovered2 , discover_tmp2 , sizeof(int) * (n + 1) );
					memcpy(pi , pi_tmp , sizeof(int) * (n + 1) );
					break;
				}
			}
		}
		if(pi[now] == 0)
			continue;
		else
			return true;

	}
	discovered1[now] = 0;
	return false;
}

void count_invariant(void)
{
	for(int i = 1 ; i <= n ; i++)
	{
		inv1[i] = degree1[i];
		inv2[i] = degree2[i];
	}
	int tmp1[maxn] , tmp2[maxn];
	for(int i = 1 ; i <= n ; i++)
	{
		tmp1[i] = (inv1[i] << 13 | inv1[i] >> 19) ^ 0xff00ff00;
		tmp2[i] = (inv2[i] << 13 | inv2[i] >> 19) ^ 0xff00ff00;
		for(int j = 1 ; j <= maxn ; j++)
		{
			if(adjmatrix1[i][j] >= 1)
				tmp1[i] += inv1[j];
			if(adjmatrix2[i][j] >= 1)
				tmp2[i] += inv2[j];
		}
	}
	for(int i = 1 ; i <= n ; i++)
	{
		inv1[i] = tmp1[i];
		inv2[i] = tmp2[i];
	}
}


bool check(void)
{
	for(int i = 1 ; i <= n ; i++)
	{
		for(int j = 1 ; j <= n ; j++)
		{
			if(adjmatrix1[i][j] > 0)
			{
				if(adjmatrix2[pi[i]][pi[j]] != adjmatrix1[i][j])
					return false;
			}
		}
	}
	return true;
}

void print()
{
	for(int i = 1 ; i < n ; i++)
		printf("%d " , pi[i]);
	printf("%d\n" , pi[n]);
	return;
}

void init(void)
{
	memset(adjmatrix1 , 0 , sizeof(int) * maxn * maxn );
	memset(adjmatrix2 , 0 , sizeof(int) * maxn * maxn );
	memset(degree1 , 0 , sizeof(int) * maxn);
	memset(degree2 , 0 , sizeof(int) * maxn);
	memset(pi , 0 , sizeof(int) * maxn);
	memset(discovered1 , 0 , sizeof(int) * maxn);
	memset(discovered2 , 0 , sizeof(int) * maxn);
}

int main(void)
{
	scanf("%d" , &T);
	while(T --)
	{
		scanf("%d %d" , &n , &m);
		init();
		int u , v;
		for(int i = 0 ; i < m ; i++)
		{
			scanf("%d %d" , &u , &v);
			adjmatrix1[u][v] ++;
			adjmatrix1[v][u] ++;
			degree1[u] ++;
			degree1[v] ++;
		}
		for(int j = 0 ; j < m ; j++)
		{
			scanf("%d %d" , &u , &v);
			adjmatrix2[u][v] ++;
			adjmatrix2[v][u] ++;
			degree2[u] ++;
			degree2[v] ++;
		}

		count_invariant();

		for(int i = 1 ; i <= n ; i++)
		{
			if(discovered1[i] == 0)
				dfs(i);
		}
		print();
	}

}