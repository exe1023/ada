#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <list>
#include <algorithm>
using namespace std;
typedef struct edg{
	int u;
	int v;
	int weight;
	bool loop;
	bool sameweight;
	bool checked;
}Edge;

class mstedge
{
public:
	int v;
	bool sameweight;
	int weight;
};

int compare(const void *a , const void *b)
{
	return( ((Edge *)a)->weight - ((Edge *)b)->weight );
}
void bridge(void);

int parent[50001];
int rnk[50001];
Edge edge[100000];
int ans ; // must number
long long int sum ; //must weight
int checkweight;
list<mstedge> *adj;
int n , m;

/* disjoint set*/
void init(int n)
{
	for(int i = 1 ; i <= n ; i++)
	{
		parent[i] = i;
		rnk[i] = 0;
	}
}

int find(int x)
{
	if(parent[x] == x)
		return x;
	else
	{
		parent[x] = find(parent[x]);
		return parent[x];
	}
}

void unite(int x , int y)
{
	x = find(x);
	y = find(y);
	if(x == y)
		return;
	if(rnk[x] < rnk[y])
		parent[x] = y;
	else
	{
		parent[y] = x;
		if(rnk[x] == rnk[y])
			rnk[x] ++;
	}
}

/* end of disjoint set*/

void kruskal(int n , int m)
{
	int edgeCount = 0;

	for(int i = 0 ; i < m ; i++)
	{
		if(edge[i].loop == true)
			continue;
		int pu = find(edge[i].u) , pv = find(edge[i].v);
		if(pu != pv)
		{
			//printf("add edge %d to %d \n" , edge[i].u , edge[i].v);
			if(edge[i].sameweight == true && edge[i].checked == false)
			{
				int j = i;
				while(edge[j].weight == edge[i].weight && j < m)
				{
					edge[j].checked = true;
					int samepu = find(edge[j].u) ,  samepv = find(edge[j].v);
					if( samepu == samepv )
					{
						edge[j].loop = true;
					}
					else
					{
						mstedge md;
						md.v = edge[j].v; md.sameweight = edge[j].sameweight; md.weight = edge[j].weight;
						adj[edge[j].u].push_back(md);
						md.v = edge[j].u;
						adj[edge[j].v].push_back(md);
						//printf("mst edge: %d %d\n" ,edge[j].u , edge[j].v);
					}
					j ++;
				}
				checkweight = edge[i].weight;
				bridge();
				//printf("findbridge\n");
			}
			if(edge[i].sameweight != true)
			{
				sum += edge[i].weight;
				ans ++;
				mstedge md;
				md.v = edge[i].v; md.sameweight = edge[i].sameweight; md.weight = edge[i].weight;
				adj[edge[i].u].push_back(md);
				md.v = edge[i].u;
				adj[edge[i].v].push_back(md);
				//printf("mst edge: %d %d\n" ,edge[i].u , edge[i].v);
			}
			unite(pu , pv);
			edgeCount ++;

		}

		if(edgeCount >= n - 1 )
		{
			break;
		}

	}
}

int edgenumber(int u , int v)
{
	list<mstedge>::iterator i;
	int ans = 0;
	for(i = adj[u].begin() ; i != adj[u].end() ; i++)
	{
		if((*i).v == v)
			ans ++;
	}
	return ans;
}

void bridgeutil(int u , bool visited[] , int disc[] , int low[] , int parent[] , int &tim)
{
	visited[u] = true;
	disc[u] = tim; low[u] = tim; tim ++;

	list<mstedge>::iterator i;
	for(i = adj[u].begin() ; i != adj[u].end() ; i++)
	{
		if(!visited[(*i).v])
		{
			parent[(*i).v] = u;
			bridgeutil((*i).v , visited , disc , low , parent , tim);
			low[u] = min(low[u] , low[(*i).v]);

			if( (low[(*i).v] > disc[u]) &&  ((*i).sameweight == true) && (*i).weight == checkweight )
			{
				//printf("bridge %d %d\n" , u , (*i).v);
				sum += (*i).weight;
				ans ++;
			}
		}
		else if( (*i).v != parent[u] || ((*i).v == parent[u] && edgenumber(u,(*i).v) >= 2) )
			low[u] = min(low[u] , disc[(*i).v]);
	}
}


void bridge(void)
{
	bool visited[50001];
	int disc[50001];
	int low[50001];
	int mstpar[50001];
	int tim = 0;
	for(int i = 0 ; i <= n ; i++)
	{
		mstpar[i] = i;
		visited[i] = false;
	}

	for(int i = 0 ; i <= n ; i++)
		if(visited[i] == false)
			bridgeutil(i , visited , disc , low , mstpar , tim);
}

int main(void)
{
	int T;
	scanf("%d" , &T);
	while(T --)
	{
		scanf("%d %d" , &n , &m);
		init(n);
		adj = new list<mstedge>[50001];
		ans = 0;
		sum = 0;
		for(int i = 0 ; i < m ; i++)
		{
			scanf("%d %d %d" , &edge[i].u , &edge[i].v , &edge[i].weight);
			edge[i].sameweight = false;
			edge[i].loop = false;
			edge[i].checked = false;
		}
		qsort(edge , m , sizeof(Edge) , compare);
		for(int i = 1 ; i < m ; i++)
		{
			if(edge[i].weight == edge[i - 1].weight)
			{
				edge[i - 1].sameweight = true;
				edge[i].sameweight = true;
			}
		}

		//printf("qsort end\n");
		kruskal(n , m);
		//bridge();
		printf("%d %lld\n" , ans , sum);
	}
}