#include <stdio.h>
#include <algorithm>
#include <vector>
#define maxperson 200000
using namespace std;
long long int n , c , e , p;
int modtable[2 * maxperson + 1]; // denote (i + j)^e mod p
long long int twotable[63];

bool e_table[63];
int e_max;

int big_mod(int x)
{
	if(e == 0)
		return 1;
	else
	{
		long long int temp_table[63];
		temp_table[0] = (long long int)x % p;
		for(int i = 1 ; i <= e_max ; i++)
			temp_table[i] = (temp_table[i - 1] * temp_table[i - 1] ) % p;
		long long int temp = 1;
		for(int i = 0 ; i <= e_max ; i++)
		{
			if(e_table[i] == true)
				temp = (temp * temp_table[i]) % p;
		}
		return temp;
	}
}

bool compare(int i , int j)
{
	if(modtable[i + j] == -1)
		modtable[i + j] = big_mod(i + j);
	long long int temp = (c * (i - j) ) % p;
	return ( (temp * modtable[i + j]) % p > (p / 2) );
}

void build_twotable(void)
{
	twotable[0] = 1;
	for(int i = 1 ; i < 63 ; i ++)
		twotable[i] = twotable[i - 1] * 2;
}

void reset(int *que)
{
	for(int i = 0 ; i < n + 1 ; i ++)
		que[i] = i;
	for(int i = 0 ; i < 2 * maxperson + 1 ; i++)
		modtable[i] = -1;
}

void decompose_e(void)
{
	int count = 62;
	long long int temp = e;
	bool flag = true;
	while(count >= 0)
	{
		if(temp >= twotable[count])
		{
			if(flag == true)
			{
				e_max = count;
				flag = false;
			}
			e_table[count] = true;
			temp -= twotable[count];
		}
		else
			e_table[count] = false;
		count -- ;
	}
}
int main(void)
{
	int T;
	scanf("%d" , &T);
	int que[maxperson + 1];
	build_twotable();
	for(int i = 0 ; i < T ; i ++)
	{
		scanf("%lld %lld %lld %lld" , &n , &c , &e , &p);
		reset(que);
		decompose_e();

		vector<int> que_vec(que + 1, que + n + 1);
		sort(que_vec.begin() , que_vec.end() , compare);
		for(int j = 0 ; j < n ; j++)
			printf("%d ", que_vec[j]);
		printf("\n");
	}
}