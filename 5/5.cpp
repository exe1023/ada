#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void pyonpyon(int *c , int n)
{
	char *number = (char *)malloc(5000);
	memset(number , '\0' , 5000);
	for(int i = 0 ; i < n ;  i++)
	{
		int diff ;

		if(i > 0)
			diff = c[i] - c[i - 1];
		else
			diff = c[i];
		int j = 0 ;
		char last;
		if(diff <= 0)
		{
			while(diff <= 0)
			{
				diff += number[j] - '0';
				last = number[j];
				number[j] = '0';
				j ++;
			}
			if(last - '0' + 1 <= diff && last - '0' + 1 < 10)
			{
				number[j - 1] = last + 1;
				diff -= (last - '0' + 1) ;
			}
			else
			{
				while(number[j] != '\0' && number[j] - '0' + 1 >= 10)
				{
					diff += number[j] - '0';
					last = number[j];
					number[j] = '0';
					j ++;
				}
				if(number[j] == '\0')
						number[j] = '0';
				number[j] += 1;
				diff --;
			}
		}

		if(diff > 0)
		{
			j = 0;
			while(diff > 0)
			{
				if(number[j] == '\0')
					number[j] = '0';

				if(diff + (number[j] - '0') >= 10 )
				{
					diff -= '9' - number[j];
					number[j] = '9';
				}
				else
				{
					number[j] += diff;
					diff = 0;
				}
				j ++;
			}
		}
		int len = strlen(number);
		for(int k = len - 1 ; k >= 0 ; k--)
			printf("%c" , number[k]);
		if(i != n - 1)
			printf(" ");
	}
	printf("\n");
}

int main(void)
{
	int T;
	int n;
	int c[2000];
	scanf("%d" , &T);
	for(int i = 0 ; i < T ; i ++)
	{
		scanf("%d" , &n);
		for(int j = 0 ; j < n ; j++)
			scanf("%d" , &c[j]);
		pyonpyon(c , n);
	}
}