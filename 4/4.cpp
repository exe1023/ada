#include <stdio.h>
#include <stdlib.h>
#include <string.h>

long long int dp[20][2][20][20][7];  //digit , presmall , 7's number , 4's number , moded
long long int solve(char *number , int len)
{
	memset((long long int *)dp , 0 ,sizeof(long long int) *20*2*20*20*7 );
	dp[0][0][0][0][0] = 1;
	for(int i = 0 ; i < len ; i++)
	{
		int D = number[i] - '0';
		for(int j = 0 ; j < 2 ; j++)
		{
			for(int k = 0 ; k <= i ; k++)
			{
				for(int q = 0 ; q <= i - k ; q++)
				{
					for(int g = 0 ; g < 7 ; g++)
					{
						for(int d = 0 ; d <= (j ? 9:D) ; d++)
						{
							if(d == 4)
								dp[i + 1][j || (d < D)][k][q + 1][( (g * 10) + d) % 7] += dp[i][j][k][q][g];
							else if(d == 7)
								dp[i + 1][j || (d < D)][k + 1][q][( (g * 10) + d) % 7] += dp[i][j][k][q][g];
							else
								dp[i + 1][j || (d < D)][k][q][( (g * 10) + d) % 7] += dp[i][j][k][q][g];
						}
					}
				}
			}
		}
	}
	long long int sum = 0;
	for(int i = 3 ; i <= len ; i++)
	{
		for(int j = 0 ; j < i ; j++)
		{
			/*if(dp[len][1][i][j][0] != 0)
				printf("i = %d , j = %d ,value = %d\n" , i , j , dp[len][1][i][j][0]);*/
			sum += dp[len][1][i][j][0];
			sum += dp[len][0][i][j][0];
		}
	}
	return sum;
}

int main(void)
{
	int T;
	long long int l , r;
	char *numberl = (char *)malloc(30);
	char *numberr = (char *)malloc(30);
	int lenl , lenr;
	scanf("%d" ,&T);
	for(int i = 0 ; i < T ; i++)
	{
		scanf("%lld %lld" , &l , &r);
		lenl = sprintf(numberl , "%lld" , l - 1);
		lenr = sprintf(numberr , "%lld" , r);
		//printf("l = %s , r = %s\n" , numberl , numberr);
		printf("%lld\n" , solve(numberr , lenr) - solve(numberl , lenl));
	}
}