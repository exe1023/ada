#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <vector>
#define maxperson 200000
using namespace std;
long long int n , c , e , p;
long long int modtable[2 * maxperson + 1]; // denote (i + j)^e mod p

bool e_table[63];
int e_max;

void big_mod(int x)
{
	if(e == 0)
	{
		modtable[x] = 1;
		return;
	}
	else if(modtable[x] != -1)
		return ;
	else if(x % 2 == 0 && x > 2)
	{
		if(modtable[2] == -1)
			big_mod(2);
		if(modtable[x/2] == -1)
			big_mod(x/2);
		modtable[x] = ( modtable[2] * modtable[x/2] ) % p;
		return;
	}
	else if(x % 3 == 0 && x > 3)
	{
		if(modtable[3] == -1)
			big_mod(3);
		if(modtable[x/3] == -1)
			big_mod(x/3);
		modtable[x] = ( modtable[3] * modtable[x/3] ) % p;
		return;
	}
	else if(x % 5 == 0 && x > 5)
	{
		if(modtable[5] == -1)
			big_mod(5);
		if(modtable[x/5] == -1)
			big_mod(x/5);
		modtable[x] = ( modtable[5] * modtable[x/5] ) % p;
		return;
	}
	else if(x % 7 == 0 && x > 7)
	{
		if(modtable[7] == -1)
			big_mod(7);
		if(modtable[x/7] == -1)
			big_mod(x/7);
		modtable[x] = ( modtable[7] * modtable[x/7] ) % p;
		return;
	}
	else
	{
		long long int temp_table[63];
		temp_table[0] = (long long int)x % p;
		if(temp_table[0] == 0)
			modtable[x] = 0;
		else
		{
			for(int i = 1 ; i <= e_max ; i++) // build E(2^x) table
				temp_table[i] = (temp_table[i - 1] * temp_table[i - 1]) % p;

			long long int temp = 1;
			for(int i = 0 ; i <= e_max ; i++) // compute remainder
			{
				if(e_table[i] == true)
					temp = (temp * temp_table[i]) % p;
			}
			modtable[x] = temp;
		}
	}
}

bool compare(int i , int j)
{
	if(modtable[i + j] == -1)
		big_mod(i + j);
	long long int temp = (c * (i - j) );
	temp %= p;
	temp = temp >= 0 ? temp: temp + p;
	return ( (temp * modtable[i + j]) % p > (p / 2) );
}

void mergesort(vector<int> &que)
{
	if(que.size() == 1)
		return;
	vector<int> left(que.begin() , que.begin() + que.size() / 2);
	vector<int> right(que.begin() + que.size() / 2 , que.end());

	mergesort(left);
	mergesort(right);

	int i = 0, j = 0 , k = 0 ;
	if (compare(left[left.size() - 1] , right[0] ) )
	{
		for(i ; j < left.size() ; i++ , j ++)
			que[i] = left[j];
		for(i ; k < right.size() ; i++ , k++)
			que[i] = right[k];
	}
	else if(compare(right[right.size() - 1] , left[0] ) )
	{
		for(i ; k < right.size() ; i++ , k++)
			que[i] = right[k];
		for(i ; j < left.size() ; i++ , j++)
			que[i] = left[j];
	}
	else
	{
		for( i ; i < que.size() ; i++)
		{
			if(k == right.size() || (j != left.size() && compare(left[j] , right[k] ) ) )
			{
				que[i] = left[j]; j++;
			}
			else
			{
				que[i] = right[k]; k++;
			}
		}
	}
}

void reset(vector<int> &que)
{
	que.resize(n);
	for(int i = 0 ; i < n  ; i ++)
		que[i] = i + 1;
	memset(modtable , -1 , sizeof(modtable));
}

void decompose_e(void)
{
	long long int test = 1 , temp = e;
	for(int i = 0 ; i < 63 ; i ++)
	{
		if(temp & test > 0)
		{
			e_max = i;
			e_table[i] = true;
		}
		else
			e_table[i] = false;
		temp >>= 1;
	}
}

int main(void)
{
	int T;
	scanf("%d" , &T);
	vector<int> que;
	que.reserve(maxperson);
	for(int i = 0 ; i < T ; i ++)
	{
		scanf("%lld %lld %lld %lld" , &n , &c , &e , &p);
		reset(que); //reset the array
		e %= (p - 1); // fermat
		decompose_e(); // analyze e's bit

		mergesort(que);
		//sort(que.begin() , que.begin() + n , compare);
		/*for(int j = 0 ; j < n - 1 ; j++)
			if(!compare(que[j] , que[j+1]))
				printf("sort error");*/
		for(int j = 0 ; j < n ; j++)
			printf("%d ", que[j]);
		printf("\n");
	}
}